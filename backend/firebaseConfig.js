var firebase = require("firebase");
var firebaseAdmin = require('firebase-admin');
var serviceAccount = require("./service-account.json");

firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccount),
    apiKey: "AIzaSyDmi8iN_XmkPao7z8THd1Dst2onG2z4wbE",
    authDomain: "yes-i-do-e63e3.firebaseapp.com",
    databaseURL: "https://yes-i-do-e63e3.firebaseio.com",
    projectId: "yes-i-do-e63e3",
    storageBucket: "yes-i-do-e63e3.appspot.com",
});

module.exports.database = firebaseAdmin.database();