module.exports = function(app) {
    var userController = require('../controllers/userController');

    app.route('/register')
        .post(userController.createUser);

    app.route('/login')
        .post(userController.getUser);    
}