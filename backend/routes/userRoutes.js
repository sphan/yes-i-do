module.exports = function (app) {
    var userController = require('../controllers/userController');

    app.route('/users/:userId')
        .get(userController.getUserById);
}