module.exports = function (app) {
    var weddingController = require('../controllers/weddingController');

    app.route('/weddings')
        .post(weddingController.createWedding);

    app.route('/weddings/:userId')
        .get(weddingController.getWeddings);
}