module.exports = function (app) {
    var eventController = require('../controllers/eventController');

    app.route('/events')
        .get(eventController.getAllEvents)
        .post(eventController.createEvent);

    app.route('/events/:eventId')
        .get(eventController.getEvent);
}