const routes = require('express').Router();
const eventRoutes = require('./eventRoutes');
const authRoutes = require('./authRoutes');
const userRoutes = require('./userRoutes');
const weddingRoutes = require('./weddingRoutes');

var jwt = require('jsonwebtoken');

module.exports = function(app) {

    // Check for unauthorised access
    function requiresLogin(req, res, next) {
        var excludePaths = [
            '/', '/about', '/login', '/register'
        ];

        // Do not do authentication checking for public paths
        if (excludePaths.indexOf(req.path) !== -1) {
            return next();
        }

        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        console.log('token: ' + token);

        // decode token
        if (token) {

            var secret = require('../config').jwtSecret;
            var options = {
                expiresIn: 1440 // 24 hours
            }

            console.log(secret);
            // verifies secret and checks exp
            jwt.verify(token, secret, options, function (err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }
    }

    app.all('*', requiresLogin);

    app.route('/', (req, res) => {
        res.status(200).json({ message: 'Connected to routes!' });
    });

    authRoutes(app);
    eventRoutes(app);
    userRoutes(app);
    weddingRoutes(app);
}