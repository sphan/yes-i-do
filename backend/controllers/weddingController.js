const mongoose = require('mongoose');
const Wedding = require('../models/wedding');

exports.getWeddings = (req, res) => {
    const userId = req.params.userId;

    console.log(`userId: ${userId}`);
    const query = { createdBy: userId };

    Wedding.find(query, (err, result) => {
        if (err) {
            console.log(`error: ${err}`);
            return res.status(404).send({ message: 'Cannot find weddings for specified user' });
        }

        return res.status(200).send(result);
    })
}

exports.createWedding = (req, res) => {
    const weddingData = req.body;

    weddingData.createdAt = Date.now();
    weddingData.lastUpdatedAt = Date.now();

    const wedding = new Wedding(weddingData);

    wedding.save((err, result) => {
        if (err) {
            console.log('error: ' + err);
            return res.status(500).send({ message: 'A problem occurred in creating a new event' });
        }

        res.status(201).send(result);
    });
}

exports.getWedding = (req, res) => {

}