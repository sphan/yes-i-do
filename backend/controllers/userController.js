var mongoose = require('mongoose');
var User = require('../models/user');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');

exports.createUser = (req, res) => {
    var userData = req.body;
    userData.createdAt = Date.now();

    var user = new User(userData);

    user.save((err, result) => {
        if (err) {
            console.log('saving user error');
            return res.status(500).send({ message: 'A problem occurred in creating a new user' });
        }

        return res.status(201).send({message: 'Register user successfully'});
    });
}

exports.getUser = (req, res) => {
    var userData = req.body;

    var query = User.findOne({ email: userData.email }).select('+password');

    query.exec((err, user) => {
        console.log(err);
        console.log(user);
        if (err) {
            console.log('err: ' + JSON.stringify(err));
            var error = new Error('Something went wrong');
            return res.status(500).send(error);
        } else if (!user) {
            var error = 'User not found';
            console.log('User not found');
            return res.status(401).send({message: error});
        }

        bcrypt.compare(userData.password, user.password, (err, result) => {
            if (err) {
                var error = new Error('User not found');
                return res.status(401).send(error);
            }
            var payload = {user: user.email};
            var secret = require('../config').jwtSecret;

            var token = jwt.sign(payload, secret, {
                expiresIn: 1440 // 24 hours
            });

            var response = {
                token: token,
                userId: user.id
            }

            res.status(200).send(response);
        });
    });
}

exports.getUserById = (req, res) => {
    var id = req.params.userId;

    console.log('userId: ' + id);

    User.findById(id, (err, user) => {
        if (err) {
            return res.status(500).send({ message: 'Something wrong happened' });
        } else if (!user) {
            return res.status(401).send({ message: 'User not found' });
        }

        return res.status(200).send(user);
    });
}