var mongoose = require('mongoose');
var Event = require('../models/event');
var userController = require('./userController');

exports.getEvent = (req, res) => {
    const eventKey = req.params.eventId;

    Event.findById(eventKey, (err, result) => {
        if (err) {
            return res.status(404).send({ message: 'Cannot find the specified event' });
        }
        return res.status(200).send(result);
    });
}

exports.getAllEvents = (req, res) => {
    var userId = req.query.userId;

    console.log('userId: ' + userId);
    var query = { createdBy: userId };

    var user = userController.getUserById(userId);

    Event.find(query, (err, result) => {
        var events = result;
        events.__count = result.length;
        res.status(200).send(events);
    });
}

exports.createEvent = (req, res) => {
    var eventData = req.body;
    eventData.createdAt = Date.now();
    eventData.lastUpdatedAt = Date.now();

    var event = new Event(eventData);

    // Create a new event
    event.save((err, result) => {
        if (err) {
            console.log('saving event error');
            return res.status(500).send({ message: 'A problem occurred in creating a new event' });
        }

        res.status(201).send(result);
    });
}