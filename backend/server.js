var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var path = require('path');
var cors = require('cors');
var morganBody = require('morgan-body');
var mongoose = require('mongoose');
var config = require('./config');

var routes = require('./routes/index');

var app = express();
var port = process.env.PORT || 3000;

var originsWhiteList = [
    'http://localhost:4200'
];

var corsOptions = {
    origin: function(origin, callback) {
        var isWhiteListed = originsWhiteList.indexOf(origin) !== -1;
        callback(null, isWhiteListed);
    },
    credentials: true
}

// mongoose.Promise = Promise;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
morganBody(app);

app.use(cors(corsOptions));

routes(app);

// app.use('/', routes);

mongoose.connect(config.dbUrl, (err) => {
    if (err) {
        console.log('err: ' + err);
    }

    // require('./routes')(app, database);

    console.log('connected to mongo');
    
});

app.listen(port);
console.log('yes-i-do REST API server started on port ' + port);


module.exports = app;