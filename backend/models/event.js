var mongoose = require('mongoose');

var eventSchema = new mongoose.Schema({
    name: String,
    createdAt: Number,
    lastUpdatedAt: Number,
    createdBy: String,
    createdByName: String,
    eventDate: String,
    eventTime: String,
    guests: [{
        guestId: String,
        guestName: String
    }], // guest IDs and guest names
    subEvents: [String] // event IDs
});

module.exports = mongoose.model('Event', eventSchema);