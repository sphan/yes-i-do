var mongoose = require('mongoose');

var weddingSchema = new mongoose.Schema({
    name: String,
    createdAt: Number,
    lastUpdatedAt: Number,
    createdBy: String,
    createdByName: String,
    weddingDate: String,
    guests: [{
        guestId: String,
        guestName: String
    }], // guest IDs and guest names
    subEvents: [String] // event IDs
});

module.exports = mongoose.model('Wedding', weddingSchema);