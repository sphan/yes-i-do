var gulp = require('gulp');
var gutil = require('gulp-util');

var jshint = require('gulp-jshint');
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var paths = {
    scripts: 'app/**/*.js',
    styles: ['./app/**/*.css', './app/**/*.less'],
    index: './app/index.html',
    partials: ['app/**/*.html', '!app/index.html'],
    distDev: './dist.dev',
    distProd: './dist.prod',
    distScriptsProd: './dist.prod/scripts',
    scriptsDevServer: 'devServer/**/*.js'
};

var pipes = {};

// pipes.orderedVendorScripts = function () {
//     return plugins.order(['jquery.js', 'angular.js']);
// };

// pipes.orderedAppScripts = function () {
//     return plugins.angularFilesort();
// };

// pipes.minifiedFileName = function () {
//     return plugins.rename(function (path) {
//         path.extname = '.min' + path.extname;
//     });
// };

// Lint Task
gulp.task('lint', function () {
    return gulp.src(paths.scripts)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Less
gulp.task('less', function () {
    return gulp.src(paths.styles)
        .pipe(less())
        .pipe(gulp.dest('dist/css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function () {
    return gulp.src(paths.scripts)
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop()) 
        .pipe(gulp.dest('dist/js'));
});

// Watch Files For Changes
gulp.task('watch', function () {
    gulp.watch(paths.scripts, ['lint', 'scripts']);
    gulp.watch(paths.styles, ['less']);
});

// Default Task
gulp.task('default', ['lint', 'less', 'scripts', 'watch']);