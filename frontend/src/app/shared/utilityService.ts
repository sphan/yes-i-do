import * as moment from 'moment';

// class UtilityService {

//     constructor() {}

//     public formatDate(date): String {
//         return moment(date).format('dddd, dd MMMM yyyy');
//     }
// }

function formatDate(date, format?): string {
    const myFormat = format ? format : 'D MMMM YYYY';
    return moment(date).format(myFormat);
}

function createRequestHeader(token): object {
    // return {headers : {
    //     'Content-Type': 'application/json',
    //     'Authorization': 'Basic ' + token,
    //     'x-access-token': token
    // }};

    return {
        headers: {
            'x-access-token': token
        }
    };
}

export {
    formatDate,
    createRequestHeader
};
