import { TestBed, inject } from '@angular/core/testing';

import { WeddingsService } from './weddings.service';

describe('WeddingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WeddingsService]
    });
  });

  it('should be created', inject([WeddingsService], (service: WeddingsService) => {
    expect(service).toBeTruthy();
  }));
});
