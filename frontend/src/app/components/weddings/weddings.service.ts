import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppSettings } from '../../app.constants';
import { Wedding } from '../../models/wedding';
import { User } from '../../models/user';
import * as utilityService from '../../shared/utilityService';

@Injectable()
export class WeddingsService {

  private headers: object;
  private user: User;

  constructor(private _http: HttpClient) {
    const token = localStorage.getItem('token');
    this.headers = utilityService.createRequestHeader(token);
    this.getUser();

    console.log(`user: ${this.user}`);
  }

  private weddingUrl = AppSettings.API_ENDPOINT + '/weddings';

  getWeddings(): Observable<Wedding[]> {
    console.log('inside getWeddings service');
    const user = localStorage.getItem('userId');
    const theUrl = this.weddingUrl + '/' + user;
    const token = localStorage.getItem('token');

    // const headers = new HttpHeaders();
    // headers.append('Content-Type', 'application/json');
    // headers.append('Authorization', 'Basic ' + token);
    // headers.append('x-access-token', token);

    // const headers = utilityService.createRequestHeader(token);

    // console.log('headers: ' + headers);

    // const options = new RequestOptions({ headers: headers});

    return this._http.get<Wedding[]>(theUrl, this.headers);
  }

  getWedding(weddingId: string): Observable<Wedding> {

    const url = this.weddingUrl + '/' + weddingId;
    return this._http.get<Wedding>(url);
  }

  createWedding(wedding): Observable<Wedding> {
    wedding.createdBy = this.user;

    // const headers = utilityService.createRequestHeader(token);

    return this._http.post<Wedding>(this.weddingUrl, Wedding, this.headers);
  }

  // To be moved elsewhere
  getUser(): void {
    const userId = localStorage.getItem('userId');
    const userUrl = AppSettings.API_ENDPOINT + '/users/' + userId;

    this._http.get<User>(userUrl, this.headers).subscribe(result => {
      if (!result) {
        console.log('no results found when getting user by id');
        return;
      }
      this.user = result;

      console.log(`result: ${result}`);
    }, err => {
      console.log('err: ' + JSON.stringify(err));
    });
  }
}
