import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WeddingsService } from './weddings.service';
import * as utilityService from '../../shared/utilityService';

import { Wedding } from '../../models/wedding';
import { User } from '../../models/user';

@Component({
  selector: 'app-weddings',
  templateUrl: './weddings.component.html',
  styleUrls: ['./weddings.component.less']
})
export class WeddingsComponent implements OnInit {

  private pageTitle = 'Weddings';
  private weddings: Wedding[];
  private weddingsCount: number;
  private user: String;

  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _weddingService: WeddingsService) { }

  ngOnInit() {
    // when the user lands on this page, redirect user
    // to home page if user does not have login token
    if (!localStorage.getItem('token') && !this.user) {
      this._router.navigate(['/']);
    }

    this.getWeddings();
    this.user = localStorage.getItem('userId');
  }

  getWeddings() {
    this._weddingService.getWeddings().subscribe(weddings => {
      console.log(weddings);
      this.weddings = weddings;
      this.weddingsCount = weddings.length;

      this.weddings.forEach((wedding) => {
        wedding.createdAt = utilityService.formatDate(wedding.createdAt);
      }, err => {
        console.log('err: ' + err);
      });
    });
  }

}
