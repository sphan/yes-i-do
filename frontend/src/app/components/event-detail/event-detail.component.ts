import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../events/events.service';
import { AppSettings } from '../../app.constants';
import { Event } from '../../models/event';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.less']
})
export class EventDetailComponent implements OnInit {

  private event: Event;

  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _eventService: EventsService) { }

  ngOnInit() {
  }

}
