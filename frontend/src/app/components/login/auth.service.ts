import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppSettings } from '../../app.constants';
import { User } from '../../models/user';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Injectable()
export class AuthService implements OnInit {

  isLoggedIn: boolean;

  constructor(private _http: HttpClient) { }

  ngOnInit() {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.

    this.isLoggedIn = false;
  }

  loginUser(loginData) {
    console.log('inside loginUser service');

    const url = AppSettings.API_ENDPOINT + '/login';
    this._http.post(url, loginData).subscribe(res => {
      const result = JSON.parse(JSON.stringify(res));

      localStorage.setItem('token', result.token);
      localStorage.setItem('userId', result.userId);

      this.isLoggedIn = true;
    });
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');

    this.isLoggedIn = false;
  }
}
