import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppSettings } from '../../app.constants';
import { User } from '../../models/user';

@Injectable()
export class LoginService {

  private isLoggedIn = false;
  currentUser: User;

  constructor(private _http: HttpClient) { }

  loginUser(loginData) {
    console.log('inside loginUser service');

    const url = AppSettings.API_ENDPOINT + '/login';
    this._http.post(url, loginData).subscribe(res => {
      const result = JSON.parse(JSON.stringify(res));

      localStorage.setItem('token', result.token);
      localStorage.setItem('userId', result.userId);

      if (result) {
        this.isLoggedIn = true;
        this.currentUser = result;
      } else {
        this.isLoggedIn = false;
        this.currentUser = null;
      }
    });
  }
}
