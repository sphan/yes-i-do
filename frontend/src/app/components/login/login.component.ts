import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user';
import { LoginService } from './login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

    private pageTitle = 'Login';

    user: any = {};

    constructor(private _route: ActivatedRoute,
                private _router: Router,
                private _loginService: LoginService) { }

    ngOnInit() {
    }

    onCancelClicked(): void {
        this._router.navigate(['/']);
    }

    onLogin(): void {
        console.log('this.user: ' + JSON.stringify(this.user));

        this._loginService.loginUser(this.user);
        this._router.navigate(['/weddings']);
    }
}
