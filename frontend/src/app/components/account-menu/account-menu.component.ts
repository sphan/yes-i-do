import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
// import { User } from '../../models/user';

import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-account-menu',
  templateUrl: './account-menu.component.html',
  styleUrls: ['./account-menu.component.less']
})
export class AccountMenuComponent implements OnInit {

  // @Input() private currentUser: User;
  private currentUser: string;

  constructor(private _router: Router,
              private _loginService: LoginService) { }

  ngOnInit() {
    this.currentUser = localStorage.getItem('token');
  }
}
