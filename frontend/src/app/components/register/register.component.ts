import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user';
import { RegisterService } from './register.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {

    pageTitle = 'Sign Up';

    user: any = {};

    constructor(private _route: ActivatedRoute,
                private _router: Router,
                private _registerService: RegisterService) { }

    ngOnInit() {
    }

    onCancelClicked(): void {
        this._router.navigate(['/']);
    }

    onRegister(): void {
        this._registerService.registerUser(this.user).subscribe(user => {
            console.log(user);
        }, err => {
            console.log('error: ' + err);
        });
    }
}
