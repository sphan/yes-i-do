import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppSettings } from '../../app.constants';
import { User } from '../../models/user';

@Injectable()
export class RegisterService {

  constructor(private _http: HttpClient) { }

  registerUser(user: User): Observable<User> {
    console.log('inside registerUser service');

    console.log('user: ' + user);

    const url = AppSettings.API_ENDPOINT + '/register';

    return this._http.post<User>(url, user);
  }

}
