import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WeddingsService } from '../weddings/weddings.service';\

import { Wedding } from '../../models/wedding';

@Component({
  selector: 'app-new-weddings',
  templateUrl: './new-weddings.component.html',
  styleUrls: ['./new-weddings.component.less']
})
export class NewWeddingsComponent implements OnInit {

  pageTitle = 'Create New Wedding';
  wedding = {};
  weddingDateFormLabel = 'Wedding Date: ';

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _weddingService: WeddingsService) { }

  ngOnInit() {
  }

  onCancelClicked(): void {
    this._router.navigate(['/weddings']);
  }

  onSave(): void {
    this._weddingService.createWedding(this.wedding).subscribe(result => {
      if (!result) {
        console.log('no results found');
        return;
      }

      this._router.navigate(['/weddings']);
    }, err => {
      console.log('err: ' + err);
    });
  }
}
