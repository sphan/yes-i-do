import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewWeddingsComponent } from './new-weddings.component';

describe('NewWeddingsComponent', () => {
  let component: NewWeddingsComponent;
  let fixture: ComponentFixture<NewWeddingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewWeddingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewWeddingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
