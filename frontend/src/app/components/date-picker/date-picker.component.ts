import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.less']
})
export class DatePickerComponent implements OnInit {

  @Input() labelName: string;

  private defaultDate;
  private selectedDate;
  private minDate;

  constructor() { }

  ngOnInit() {
    this.defaultDate = moment().format('DD MMM YYYY');
    this.minDate = {
      year: this.defaultDate.year(),
      month: this.defaultDate.month(),
      day: this.defaultDate.day(),
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.selectedDate = moment(this.selectedDate).format('DD MMM YYYY');
  }
}
