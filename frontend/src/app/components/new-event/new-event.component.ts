import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../events/events.service';

import { Event } from '../../models/event';

@Component({
  selector: 'app-new-event',
  templateUrl: './new-event.component.html',
  styleUrls: ['./new-event.component.less']
})
export class NewEventComponent implements OnInit {

  pageTitle = 'Create New Event';
  event = {};
  eventDateFormLabel = 'Event Date:';

  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _eventService: EventsService) { }

  ngOnInit() {
  }

  onCancelClicked(): void {
    this._router.navigate(['/events']);
  }

  onSave(): void {
    this._eventService.createEvent(this.event).subscribe(result => {
      console.log('result: ' + result);

      if (!result) {
        console.log('no results found');
        return;
      }

      this._router.navigate(['/events']);
    }, err => {
      console.log('err: ' + err);
    });
  }

}
