import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppSettings } from '../../app.constants';
import { Event } from '../../models/event';
import { User } from '../../models/user';
import * as utilityService from '../../shared/utilityService';

@Injectable()
export class EventsService {

  private headers: object;
  private user: User;

  constructor(private _http: HttpClient) {
    const token = localStorage.getItem('token');
    this.headers = utilityService.createRequestHeader(token);
    this.getUser();
  }

  private eventUrl = AppSettings.API_ENDPOINT + '/events';

  getEvents(): Observable<Event[]> {
    console.log('inside getEvents service');
    const user = localStorage.getItem('userId');
    const theUrl = this.eventUrl + '?userId=' + user;
    const token = localStorage.getItem('token');

    // const headers = new HttpHeaders();
    // headers.append('Content-Type', 'application/json');
    // headers.append('Authorization', 'Basic ' + token);
    // headers.append('x-access-token', token);

    // const headers = utilityService.createRequestHeader(token);

    // console.log('headers: ' + headers);

    // const options = new RequestOptions({ headers: headers});

    return this._http.get<Event[]>(theUrl, this.headers);
  }

  getEvent(eventId: string): Observable<Event> {

    const url = this.eventUrl + '/' + eventId;
    return this._http.get<Event>(url);
  }

  createEvent(event): Observable<Event> {
    event.createdBy = this.user;

    // const headers = utilityService.createRequestHeader(token);

    return this._http.post<Event>(this.eventUrl, event, this.headers);
  }

  // To be moved elsewhere
  getUser(): void {
    const userId = localStorage.getItem('userId');
    const userUrl = AppSettings.API_ENDPOINT + '/users/' + userId;

    this._http.get<User>(userUrl, this.headers).subscribe(result => {
      if (!result) {
        console.log('no results found when getting user by id');
        return;
      }
      this.user = result;
    }, err => {
      console.log('err: ' + JSON.stringify(err));
    });
  }
}
