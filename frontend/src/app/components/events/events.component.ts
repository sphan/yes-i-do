import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from './events.service';
import * as utilityService from '../../shared/utilityService';

import { Event } from '../../models/event';

@Component({
    selector: 'app-events',
    templateUrl: './events.component.html',
    styleUrls: ['./events.component.less']
})
export class EventsComponent implements OnInit {

    pageTitle = 'Events';
    events: Event[];
    user: string;
    @Output() row = new EventEmitter<Event>();

    columns = [
        'Event Name',
        'Event Date',
        'Created By',
        'Created Date'
    ];

    constructor(private _route: ActivatedRoute,
                private _router: Router,
                private _eventService: EventsService) { }

    ngOnInit() {
        this.getEvents();
        this.user = localStorage.getItem('userId');

        console.log('user: ' + this.user);

        if (!localStorage.getItem('token') && !this.user) {
            this._router.navigate(['/']);
        }
    }

    getEvents(): void {
        this._eventService.getEvents().subscribe(events => {
            console.log(events);
            this.events = events;

            this.events.forEach(function(event) {
                event.createdAt = utilityService.formatDate(event.createdAt);
            }, err => {
                console.log('err: ' + err);
            });
        });
    }

    showDetails(eventId: string) {
        console.log('eventId: ' + eventId);
        this._eventService.getEvent(eventId).subscribe(event => {
            console.log('event: ' + event);
        }, err => {
            console.log('err: ' + err);
        });
    }
}
