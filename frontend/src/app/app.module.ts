import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { EventsComponent } from './components/events/events.component';
import { NewEventComponent } from './components/new-event/new-event.component';
import { EventDetailComponent } from './components/event-detail/event-detail.component';
import { WeddingsComponent } from './components/weddings/weddings.component';

import { EventsService } from './components/events/events.service';
import { RegisterService } from './components/register/register.service';
import { LoginService } from './components/login/login.service';
import { WeddingsService } from './components/weddings/weddings.service';
import { AccountMenuComponent } from './components/account-menu/account-menu.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { NewWeddingsComponent } from './components/new-weddings/new-weddings.component';

const appRoutes: Routes = [
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'events',
    component: EventsComponent
  },
  {
    path: 'weddings',
    component: WeddingsComponent
  },
  {
    path: 'new-event',
    component: NewEventComponent
  },
  {
    path: 'new-wedding',
    component: NewWeddingsComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    EventsComponent,
    NewEventComponent,
    EventDetailComponent,
    WeddingsComponent,
    AccountMenuComponent,
    DatePickerComponent,
    NewWeddingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [EventsService, RegisterService, LoginService, WeddingsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
