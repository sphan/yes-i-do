import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'Yes I Do';
  currentUser: any;

  constructor(private _router: Router) { }

  ngOnInit() {
    this.currentUser = localStorage.getItem('token');

    console.log('this.currentUser: ' + this.currentUser);
  }

  isLoggedIn(): boolean {
    return localStorage.getItem('token') ? true : false;
  }

  logOut(): void {
    console.log('logging out user');

    // remove user related data
    localStorage.removeItem('token');
    localStorage.removeItem('userId');

    // redirect user back to home page
    this._router.navigate(['/']);
  }

}
