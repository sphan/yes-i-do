export class Wedding {
    id: string;
    name: string;
    createdBy: string;
    createdByName: string;
    createdAt: string;
    lastUpdatedAt: string;
    eventDate: string;
    // tslint:disable-next-line:eofline
}