export class User {
    id: string;
    name: string;
    firstName: string;
    lastName: string;
    email: string;
}
