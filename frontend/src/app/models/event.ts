export class Event {
    id: string;
    name: string;
    createdBy: string;
    createdByName: string;
    createdAt: string;
    lastUpdatedAt: string;
    eventDate: string;
    eventTime: string;
// tslint:disable-next-line:eofline
}